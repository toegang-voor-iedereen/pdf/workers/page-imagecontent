import unittest
from utils import read_values_from_record_id


class TestReadValuesFromRecordId(unittest.TestCase):
    def test_valid_record_id(self):
        """Test with a valid record ID containing all expected parts"""
        record_id = "page123|||doc456|||7"
        page_id, document_id, page_number = read_values_from_record_id(record_id)

        self.assertEqual(page_id, "page123|||doc456|||7")
        self.assertEqual(document_id, "doc456")
        self.assertEqual(page_number, 7)
        self.assertIsInstance(page_number, int)

    def test_record_id_with_special_characters(self):
        """Test with a record ID containing special characters"""
        record_id = "page#123|||doc@456$|||9"
        page_id, document_id, page_number = read_values_from_record_id(record_id)

        self.assertEqual(page_id, "page#123|||doc@456$|||9")
        self.assertEqual(document_id, "doc@456$")
        self.assertEqual(page_number, 9)

    def test_invalid_page_number_format(self):
        """Test when page number is not a valid integer"""
        record_id = "page123|||doc456|||invalid"

        with self.assertRaises(ValueError):
            read_values_from_record_id(record_id)

    def test_empty_record_id(self):
        """Test with an empty record ID string"""
        record_id = ""

        with self.assertRaises(IndexError):
            read_values_from_record_id(record_id)

    def test_missing_parts(self):
        """Test with a record ID missing required parts"""
        record_id = "page123|||doc456"

        with self.assertRaises(IndexError):
            read_values_from_record_id(record_id)
