import unittest
from unittest.mock import Mock, MagicMock, patch, ANY
from kimiworker import KimiLogger, WorkerJob, RemoteFileStorageInterface
from main import (
    get_job_handler,
)
from utils import (
    read_image_from_file,
    read_values_from_record_id,
)


class TestFunctions(unittest.TestCase):

    @patch("cv2.imread")
    def test_read_image_from_file(self, mock_imread):
        # Mock the return value of cv2.imread
        mock_imread.return_value = MagicMock()
        mock_imread.return_value.shape = (100, 200, 3)

        # Call the function
        page_image, page_height, page_width = read_image_from_file("dummy_path")

        # Assertions
        mock_imread.assert_called_with("dummy_path")
        self.assertEqual(page_height, 100)
        self.assertEqual(page_width, 200)

    def test_read_values_from_record_id(self):
        record_id = "page|||mocked-document-id|||3"
        page_id, document_id, page_number = read_values_from_record_id(record_id)

        # Assertions
        self.assertEqual(page_id, record_id)
        self.assertEqual(document_id, "mocked-document-id")
        self.assertEqual(page_number, 3)

    @patch("main.cv2")
    @patch("main.utils")
    @patch("main.RemoteFileStorageInterface")
    @patch("main.PublishMethod")
    @patch("main.KimiLogger")
    @patch("main.WorkerJob")
    def test_job_handler(
        self,
        MockWorkerJob,
        MockKimiLogger,
        MockPublishMethod,
        MockRemoteFileStorage,
        MockUtils,
        MockCv2,
    ):
        # Setup mocks
        mock_job: WorkerJob = {
            "attributes": {
                "regions": {
                    "values": [
                        {
                            "contentResult": [
                                {
                                    "bbox": {
                                        "top": 0.1,
                                        "left": 0.2,
                                        "bottom": 0.3,
                                        "right": 0.4,
                                    },
                                    "labels": [
                                        {"name": "picture", "confidence": 0.987654321}
                                    ],
                                    "attributes": {},
                                    "children": [],
                                },
                                {
                                    "bbox": {
                                        "top": 0.11,
                                        "left": 0.22,
                                        "bottom": 0.33,
                                        "right": 0.44,
                                    },
                                    "labels": [{"name": "text", "confidence": 0.7}],
                                    "attributes": {},
                                    "children": [],
                                },
                            ]
                        }
                    ]
                }
            },
            "recordId": "page|||document|||3",
            "bucketName": "bucket-name",
            "filename": "filename",
        }

        mock_log = MockKimiLogger.return_value
        mock_publish = MockPublishMethod.return_value
        mock_remote_file_storage = MockRemoteFileStorage.return_value

        # Mock the image read
        mock_image = MagicMock()
        mock_image.shape = (1000, 800, 3)
        MockUtils.get_regions.return_value = [
            {
                "bbox": {
                    "top": 0.1,
                    "left": 0.2,
                    "bottom": 0.3,
                    "right": 0.4,
                },
                "labels": [{"name": "picture", "confidence": 0.987654321}],
                "attributes": {},
                "children": [],
            },
        ]
        MockUtils.read_image_from_file.return_value = (mock_image, 1000, 800)
        MockUtils.read_values_from_record_id.return_value = ("page", "document", 3)
        picture_encoding = MagicMock()
        MockCv2.imencode.return_value = [True, picture_encoding]
        picture_encoding.tobytes.return_value = str.encode("mocked1")

        handler = get_job_handler()
        handler(
            mock_log,
            mock_job,
            "job_id",
            "local_file_path",
            mock_remote_file_storage,
            mock_publish,
        )

        # Check calls and state changes
        self.assertTrue(mock_log.child.called)
        self.assertTrue(mock_publish.called)
        self.assertTrue(mock_remote_file_storage.put_object.called)

    def test_job_handler_result(self):
        MOCK_JOB_ID = "test_job_id"
        MOCK_LOCAL_FILE_PATH = "test/test.jpg"

        mock_remote_file_storage = Mock(spec=RemoteFileStorageInterface)
        mock_publish = Mock()
        mock_log = Mock(spec=KimiLogger)
        mock_job: WorkerJob = {
            "recordId": "mock|||1234567890abcdef|||2",
            "bucketName": "mocked-bucket",
            "filename": "mocked-file.jpg",
            "attributes": {
                "regions": {
                    "attribute": "regions",
                    "expectedValues": 1,
                    "isComplete": True,
                    "bestJobId": "mocked-job-id",
                    "values": [
                        {
                            "traceId": "mocked-trace-id",
                            "jobId": "mocked-job-id",
                            "recordId": "mocked-record-id",
                            "success": True,
                            "confidence": 100,
                            "contentResult": [
                                {
                                    "classification": "application/x-nldoc.region",
                                    "bbox": {
                                        "left": 0.1,
                                        "top": 0.2,
                                        "right": 0.3,
                                        "bottom": 0.4,
                                    },
                                    "confidence": 0.987654321,
                                    "attributes": {},
                                    "labels": [
                                        {"name": "picture", "confidence": 0.987654321}
                                    ],
                                    "children": [],
                                }
                            ],
                        }
                    ],
                }
            },
        }

        expected_content_definitions = [
            {
                "classification": "application/x-nldoc.page",
                "labels": [],
                "confidence": 100,
                "bbox": {"top": 0, "bottom": 1, "left": 0, "right": 1},
                "attributes": {
                    "pageNumber": 2,
                },
                "children": [
                    {
                        "classification": "application/x-nldoc.element.figure",
                        "labels": [{"name": "picture", "confidence": 0.987654321}],
                        "confidence": 0.987654321,
                        "bbox": {"left": 0.1, "top": 0.2, "right": 0.3, "bottom": 0.4},
                        "attributes": {
                            "id": ANY,
                            "document_id": "1234567890abcdef",
                            "file_extension": "jpg",
                            "content_type": "image/jpg",
                            "page_id": "mock|||1234567890abcdef|||2",
                            "width": 331,
                            "height": 469,
                            "minio_bucket_name": "staticassets",
                            "minio_file_path": ANY,
                        },
                        "children": [],
                    }
                ],
            }
        ]
        job_handler = get_job_handler()

        job_handler(
            log=mock_log,
            job=mock_job,
            job_id=MOCK_JOB_ID,
            local_file_path=MOCK_LOCAL_FILE_PATH,
            remote_file_storage=mock_remote_file_storage,
            publish=mock_publish,
        )

        # Check if publish was called correctly
        mock_publish.assert_called_once_with(
            "contentResult",
            expected_content_definitions,
            success=True,
            confidence=0.987654321,
        )


if __name__ == "__main__":
    unittest.main()
