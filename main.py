import os, sys, uuid, math
import cv2
from io import BytesIO

from kimiworker import (
    JobHandler,
    KimiLogger,
    WorkerJob,
    PublishMethod,
    Worker,
    ContentDefinition,
    BoundingBox,
    RemoteFileStorageInterface,
)
from kimiworker.utils.content_definition import has_label
from typing import List
import utils

#
static_assets_bucket_name = os.getenv("MINIO_STATIC_ASSETS_BUCKET_NAME", "staticassets")
numberOfConcurrentJobs = int(os.getenv("CONCURRENT_JOBS", "1"))
image_extension = "jpg"
image_jpeg_quality = 80
image_mimetype = f"image/{image_extension}"


def get_job_handler() -> JobHandler:
    """Get the job handler"""

    def job_handler(
        log: KimiLogger,
        job: WorkerJob,
        job_id: str,
        local_file_path: str,
        remote_file_storage: RemoteFileStorageInterface,
        publish: PublishMethod,
    ):
        """Handle getting regions from page image"""

        # Extract values from the record ID
        record_id = job.get("recordId")
        page_id, document_id, page_number = utils.read_values_from_record_id(record_id)

        # Read the image from the local file path
        page_image, page_height, page_width = utils.read_image_from_file(
            local_file_path
        )

        log = log.child(
            {
                "page_id": page_id,
                "document_id": document_id,
                "page_number": page_number,
                "page_size": {"width": page_width, "height": page_height},
            }
        )

        # Collect all content from the regions
        all_content = utils.get_regions(job=job)
        log.trace(f"Received {len(all_content)} regions")

        # Filter content to get only pictures
        picture_content = list(
            filter(
                lambda item: has_label(item, "picture"),
                all_content,
            )
        )

        log.trace(f"Found {len(picture_content)} pictures")

        log = log.child(
            {
                "found_regions_count": len(all_content),
                "found_pictures_count": len(picture_content),
            }
        )

        mean_confidence = 100

        if len(picture_content) > 0:

            for picture in picture_content:
                # Calculate the bounding box dimensions on the page image
                bbox: BoundingBox = picture.get("bbox")
                top = math.floor(bbox["top"] * page_height)
                left = math.floor(bbox["left"] * page_width)
                bottom = math.ceil(bbox["bottom"] * page_height)
                right = math.ceil(bbox["right"] * page_width)
                width = right - left
                height = bottom - top

                # Generate a unique ID for the picture, and store all data in the attributes
                picture_id = str(uuid.uuid4())

                remote_image_path = (
                    f"{document_id}/images/{picture_id}.{image_extension}"
                )

                picture.get("attributes").update(
                    {
                        "id": picture_id,
                        "document_id": document_id,
                        "file_extension": image_extension,
                        "content_type": image_mimetype,
                        "page_id": page_id,
                        "width": width,
                        "height": height,
                        "minio_bucket_name": static_assets_bucket_name,
                        "minio_file_path": remote_image_path,
                    }
                )

                # Extract a picture with the given bbox from the page image
                picture_image = page_image[top:bottom, left:right]

                log.trace(
                    f"Uploading to {static_assets_bucket_name}/{remote_image_path}"
                )

                # Encode the picture and upload it to the Remote File Storage
                picture_bytes = str.encode("")
                picture_encoding = cv2.imencode(
                    f".{image_extension}",
                    picture_image,
                    [
                        cv2.IMWRITE_JPEG_QUALITY,
                        image_jpeg_quality,
                        cv2.IMWRITE_JPEG_PROGRESSIVE,
                        1,
                        cv2.IMWRITE_JPEG_OPTIMIZE,
                        1,
                    ],
                )
                if picture_encoding:
                    picture_bytes = picture_encoding[1].tobytes()

                remote_file_storage.put_object(
                    bucket_name=static_assets_bucket_name,
                    object_name=remote_image_path,
                    data=BytesIO(picture_bytes),
                    length=len(picture_bytes),
                    content_type=image_mimetype,
                )

            mean_confidence = utils.calculate_mean_confidence(picture_content)

        page_content: List[ContentDefinition] = [
            {
                "classification": "application/x-nldoc.page",
                "labels": [],
                "confidence": 100,
                "bbox": {"top": 0, "bottom": 1, "left": 0, "right": 1},
                "attributes": {"pageNumber": page_number},
                "children": [
                    {
                        "classification": "application/x-nldoc.element.figure",
                        "labels": content.get("labels", []),
                        "confidence": content.get("confidence", 0),
                        "bbox": content.get("bbox"),
                        "attributes": content.get("attributes", {}),
                        "children": content.get("children", []),
                    }
                    for content in picture_content
                ],
            }
        ]

        log = log.child({"page_content": page_content})

        log.info("Job done")

        publish(
            "contentResult",
            page_content,
            success=True,
            confidence=mean_confidence,
        )

    return job_handler


if __name__ == "__main__":  # pragma: no cover
    try:
        worker = Worker(get_job_handler(), "worker-page-imagecontent", True)
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
