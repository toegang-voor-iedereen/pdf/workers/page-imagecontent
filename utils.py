from statistics import mean
from kimiworker import ContentDefinition, WorkerJob
from typing import List
from itertools import chain
from pathdict import PathDict
import cv2


def calculate_mean_confidence(content_result: list[ContentDefinition]) -> float:
    """
    Calculate the mean confidence for the content regions found

    Args:
        - content_result (list[ContentDefinition]): List of content definitions

    Returns:
        - float: the mean confidence of all result items
    """
    return mean([content.get("confidence", 0) for content in content_result])


def get_regions(job: WorkerJob) -> List[ContentDefinition]:
    """Gets regions from worker job"""
    attributes = PathDict(job.get("attributes"))
    values = attributes["regions.values"]

    if values is None:
        return []

    # get contentResult from each value
    values = [value["contentResult"] for value in values]

    # Flatten the array and return it
    return list(chain.from_iterable(values))


def read_values_from_record_id(record_id: str):
    """
    Extracts and returns the page ID, document ID, and page number from a given record ID.

    Args:
        record_id (str): The record ID containing page ID, document ID, and page number.

    Returns:
        tuple: A tuple containing the page ID, document ID, and page number.
    """
    page_id_parts = record_id.split("|||")
    document_id = page_id_parts[1]
    page_number = int(page_id_parts[2])

    return record_id, document_id, page_number


def read_image_from_file(file_path: str):
    """
    Reads an image from a file and returns the image along with its dimensions.

    Args:
        file_path (str): The path to the image file.

    Returns:
        tuple: A tuple containing the image, its height, and its width.
    """
    # Read the image from the given file path
    page_image = cv2.imread(file_path)

    # Get the dimensions of the image
    page_height, page_width, _ = page_image.shape

    return page_image, page_height, page_width
