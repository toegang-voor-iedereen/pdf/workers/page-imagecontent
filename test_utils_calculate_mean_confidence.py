from statistics import StatisticsError
import pytest
from utils import calculate_mean_confidence
from kimiworker import ContentDefinition
from typing import List


def test_calculate_mean_confidence_normal_case():
    """Test calculation with normal confidence values"""
    test_data: List[ContentDefinition] = [
        {
            "classification": "text",
            "confidence": 0.8,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        },
        {
            "classification": "text",
            "confidence": 0.9,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        },
        {
            "classification": "text",
            "confidence": 0.7,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        },
    ]

    result = calculate_mean_confidence(test_data)
    assert result == 0.8


def test_calculate_mean_confidence_empty_list():
    """Test calculation with empty list"""
    test_data: List[ContentDefinition] = []

    with pytest.raises(StatisticsError):
        calculate_mean_confidence(test_data)


def test_calculate_mean_confidence_missing_confidence():
    """Test calculation when some items are missing confidence"""
    test_data: List[ContentDefinition] = [
        {
            "classification": "text",
            "confidence": 0.8,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        },
        {
            "classification": "text",
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        },  # type: ignore - Missing "confidence"
        {
            "classification": "text",
            "confidence": 0.7,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        },
    ]

    result = calculate_mean_confidence(test_data)
    assert result == 0.5  # (0.8 + 0 + 0.7) / 3


def test_calculate_mean_confidence_zero_values():
    """Test calculation with zero confidence values"""
    test_data: List[ContentDefinition] = [
        {
            "classification": "text",
            "confidence": 0.0,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        },
        {
            "classification": "text",
            "confidence": 0.0,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        },
    ]

    result = calculate_mean_confidence(test_data)
    assert result == 0.0


def test_calculate_mean_confidence_single_item():
    """Test calculation with a single item"""
    test_data: List[ContentDefinition] = [
        {
            "classification": "text",
            "confidence": 0.95,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": [],
        }
    ]

    result = calculate_mean_confidence(test_data)
    assert result == 0.95
