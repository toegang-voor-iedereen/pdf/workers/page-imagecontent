--extra-index-url https://gitlab.com/api/v4/projects/48255673/packages/pypi/simple

kimiworker==4.5.0
opencv-python-headless==4.11.0.86
opencv-contrib-python-headless==4.11.0.86
pytest
path-dict==4.0.0