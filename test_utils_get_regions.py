import unittest
from unittest.mock import Mock, patch
from utils import get_regions
from pathdict import PathDict


class TestGetRegions(unittest.TestCase):
    def setUp(self):
        """Set up test cases"""
        # Create a mock WorkerJob for reuse in tests
        self.mock_job = Mock()

    def test_get_regions_with_none_values(self):
        """Test when regions.values is None"""
        # Configure mock to return None for regions.values
        self.mock_job.get.return_value = {"regions": {"values": None}}

        # Call function and verify empty list is returned
        result = get_regions(self.mock_job)
        self.assertEqual(result, [])

        # Verify mock was called correctly
        self.mock_job.get.assert_called_once_with("attributes")

    def test_get_regions_with_empty_values(self):
        """Test when regions.values is an empty list"""
        # Configure mock to return empty list
        self.mock_job.get.return_value = {"regions": {"values": []}}

        # Call function and verify empty list is returned
        result = get_regions(self.mock_job)
        self.assertEqual(result, [])

    def test_get_regions_with_single_content_result(self):
        """Test when regions.values contains a single content result"""
        # Sample content result
        content_result = [{"type": "text", "confidence": 0.95}]

        # Configure mock to return a single value
        self.mock_job.get.return_value = {
            "regions": {"values": [{"contentResult": content_result}]}
        }

        # Call function and verify result
        result = get_regions(self.mock_job)
        self.assertEqual(result, content_result)

    def test_get_regions_with_multiple_content_results(self):
        """Test when regions.values contains multiple content results"""
        # Sample content results
        content_result1 = [{"type": "text", "confidence": 0.95}]
        content_result2 = [{"type": "table", "confidence": 0.85}]

        # Configure mock to return multiple values
        self.mock_job.get.return_value = {
            "regions": {
                "values": [
                    {"contentResult": content_result1},
                    {"contentResult": content_result2},
                ]
            }
        }

        # Call function and verify results are flattened correctly
        result = get_regions(self.mock_job)
        expected = content_result1 + content_result2
        self.assertEqual(result, expected)

    def test_get_regions_with_nested_content_results(self):
        """Test when content results contain nested arrays"""
        # Sample nested content results
        content_result1 = [
            {"type": "text", "confidence": 0.95},
            [{"type": "nested", "confidence": 0.80}],
        ]
        content_result2 = [{"type": "table", "confidence": 0.85}]

        # Configure mock to return nested values
        self.mock_job.get.return_value = {
            "regions": {
                "values": [
                    {"contentResult": content_result1},
                    {"contentResult": content_result2},
                ]
            }
        }

        # Call function and verify results are flattened correctly
        result = get_regions(self.mock_job)
        # The chain.from_iterable should flatten one level
        expected = list(content_result1) + content_result2
        self.assertEqual(result, expected)
