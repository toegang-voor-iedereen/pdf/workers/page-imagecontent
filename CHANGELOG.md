# Changelog

## [2.4.5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.4.4...2.4.5) (2025-03-06)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.4 ([7357014](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/7357014fb14ea911a7fad4f3edbb083aeeadb949))

## [2.4.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.4.3...2.4.4) (2025-03-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.3 ([2f50192](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/2f50192ea3d23429bfe9135f0cb82ebd4af09b5e))

## [2.4.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.4.2...2.4.3) (2025-03-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.2 ([f44b046](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/f44b046ef328af2811b71182f00338e1506cabf7))

## [2.4.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.4.1...2.4.2) (2025-03-05)


### Bug Fixes

* remove debug prints ([0dcd85e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/0dcd85e223d7634ca3b61fc1e77e83386b8aab8e))

## [2.4.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.4.0...2.4.1) (2025-01-20)


### Bug Fixes

* update base worker ([743dc31](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/743dc313dba4b518265dcadbfad51d252024ec5e))

# [2.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.3.0...2.4.0) (2025-01-15)


### Features

* use JPEG instead of PNG ([281372f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/281372fe260268524d55ae34079e231b65a9bc97))

# [2.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.2.1...2.3.0) (2024-12-16)


### Features

* **deps:** kimiworker 4.4.0, removed minio dependency ([877bfb9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/877bfb9f550b6ec01457836e0953c193ce00b4a3))

## [2.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.2.0...2.2.1) (2024-11-21)


### Bug Fixes

* remove tables as images ([940711a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/940711a832f0699d284ad88077dcf2b7b8e423f1))

# [2.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.1.0...2.2.0) (2024-11-19)


### Features

* force release with kimiworker v3.6.0 ([8d13e45](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/8d13e45cc80315f8741737b4acc6db616fe96b4d))

# [2.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/2.0.0...2.1.0) (2024-11-14)


### Features

* present tables as images ([62158de](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/62158de59fadbbc7319818f9ff7e309819212fa4))

# [2.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.12.0...2.0.0) (2024-11-12)


### Bug Fixes

* removed debug print ([7627b3b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/7627b3bdd88aef7030cf31d48aaf91e1b2309b51))
* revert to 1.12.0 ([c1b1e01](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/c1b1e01fba2a24d0897e74f10b201a1f83fa4823))
* upped version ([1875ff6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/1875ff6fcc0a47071b9c664ea19073a3b5b1297d))
* use correct has_label ([241d9d5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/241d9d5853480897e557294ca3483f7545d43d10))


### Features

* add image minio bucket and file information to attributes ([9c8d02c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/9c8d02c0b8dd70c8bd7bea66eb44aa0f95301454))
* back to ContentDefinition instead of nldocspec ([d7da61c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/d7da61cabb0846a5c5c6e1edbba10965849975a8))
* back to ContentDefinition, undo nldocspec work ([8be0f97](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/8be0f97f693d64deb777c475ea868c9569dfde91))
* kimiworker 3.5.0 ([8e7da54](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/8e7da549a6663aa17dffcdf45071856b848f8c35))
* use kimiworker 2.15 ([b38fea9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/b38fea9b90fcec780a5e57029cb4f6eed25a8830))
* use nldocspec objects ([1cbb15f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/1cbb15fa61c53b1241cbb696d80c4dc04fe7bc50))
* worker now communicates in nldocspec ([5ac2df9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/5ac2df9e96017e41592eb1e6a647b8da0aafcb02))


### BREAKING CHANGES

* Now removed nldocspec

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.11.0...1.12.0) (2024-09-11)

### Features

- added volume mount for /tmp ([bbc05b6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/bbc05b633b7883462b222f6de481b05b2911c3d5))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.10.0...1.11.0) (2024-09-04)

### Features

- added securityContext to container and initContainers ([e393198](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/e3931983c369870a01f5136d664ffc9670938a39))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.9.0...1.10.0) (2024-08-28)

### Bug Fixes

- cleanup ([efab3b4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/efab3b4fcbafdd6de98c71dbb8da08db116f9fcd))

### Features

- use new kimiworker to connect to station 4.0.0 and up ([55cd5a7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/55cd5a70367ad920bd0e35c035bb780ff21557f5))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.8.0...1.9.0) (2024-08-28)

### Features

- use new kimiworker to connect to station 4.0.0 and up ([d524cc4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/d524cc469a209b73217dcc0d3ea17cc8db1eed4d))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.7.0...1.8.0) (2024-07-04)

### Bug Fixes

- attempt sigint fix ([e8a8ef8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/e8a8ef8f3cfbf9bf1c5fafbe5abd1bfec5ce3dfa))

### Features

- support initContainers in helm values ([88bda7a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/88bda7a5349c3cbeb0fe1536a20328fc0172517e))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.6.0...1.7.0) (2024-06-05)

### Features

- support initContainers in helm values ([9e3028d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/9e3028dde2d0ed2052319329dc7f2482b1caf09e))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.5.0...1.6.0) (2024-06-05)

### Features

- merge branch 'develop' - ENTRYPOINT instead of CMD ([cc5e482](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/cc5e482b2f777b881077b9adb9381e71e498ca97))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/compare/1.4.3...1.5.0) (2024-06-04)

### Bug Fixes

- removed old test file ([4663301](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/46633018b6d4b15b7cc6b7162ba88863e3b5daa6))

### Features

- correct worker name in chart ([3d23d9d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images/commit/3d23d9d247728ac8643efc36e9c074a7c400b0af))

## [1.4.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/compare/1.4.2...1.4.3) (2024-05-30)

### Bug Fixes

- removed gitattributes for lfs ([461820b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/461820b5fac102e3e6965f0a0ef440a07366f544))

## [1.4.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/compare/1.4.1...1.4.2) (2024-05-29)

### Bug Fixes

- removed pip install from Dockerfile ([9411658](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/9411658e66a3320c3418bbbf02f04a2ba5d5b454))
- removed pip install from pre-test run ([065fa27](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/065fa27e49cf117e01e2679401063b084c699391))

## [1.4.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/compare/1.4.0...1.4.1) (2024-05-29)

### Bug Fixes

- removed old files ([f6b4a0b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/f6b4a0b85e9c36f6a98ee1edc347650e16a79e06))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/compare/1.3.1...1.4.0) (2024-05-29)

### Bug Fixes

- confidence times 100 ([4bd9805](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/4bd9805527da081528cecdab594232df49fba9e5))
- remove unused requirements from yolo ([53ddade](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/53ddade834abdbd42d8176b8ecf38c9d6f23ced8))
- removed old tools and fixed unit tests ([4c85293](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/4c852930b0087cb1b04d7cd68e61f089386b3ace))
- unit tests fixed ([ba9f5f2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/ba9f5f2b7aa5d248f26254231aa76bfe76bc9b75))

### Features

- detect images and post them to minio static assets bucket ([4ee1785](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-imagecontent/commit/4ee1785e61fbec79852077801856a0a577544fb1))

## [1.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.3.0...1.3.1) (2024-05-17)

### Bug Fixes

- use proper version of dill ([0e80736](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/0e807364cff9d3d2911fc74ce3b23374c7a619d6))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.2.0...1.3.0) (2024-05-16)

### Features

- use cpu torch ([412757e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/412757e6c3f4e04d662d8c574e18cc58f0f57218))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.1.0...1.2.0) (2024-05-16)

### Features

- use cpu versions of torch ([fb5656c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/fb5656cbe45fff3f02e731baacf55ef78a206c33))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.0.2...1.1.0) (2024-05-16)

### Features

- rename worker ([1cedc88](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/1cedc88f21c535b3f7f027e13bf801c1db84aea6))

## [1.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.0.1...1.0.2) (2024-05-16)

### Bug Fixes

- add gcc compiler to image ([f4498a2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/f4498a24c322ef1405937f897ed07bb4375ca0a6))

## [1.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.0.0...1.0.1) (2024-05-16)

### Bug Fixes

- use python 3.12 slim ([fae93c8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/fae93c8dd5ae998ffbf84c731ad824d7dc3edbbe))
- use pytorch image ([1b8c272](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/1b8c27229cd9b5a12f91be0c49dba787f9c014ec))

# 1.0.0 (2024-05-16)

### Bug Fixes

- add torch and torchvision ([127ba8a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/127ba8a6f9d7da52a6fa2133111f7339dcdf45f6))

### Features

- add model ([45c5fed](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/45c5fedd7cd9635dc410795a6c77ebafa8adaaf7))
- yolov8 worker ([e49a89c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/e49a89c00716e4bf98f4821c0ceee9bb0fcedef2))
