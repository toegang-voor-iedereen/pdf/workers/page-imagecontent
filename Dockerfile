# syntax=docker/dockerfile:1

FROM python:3.13-slim

RUN apt-get update && apt-get -y install gcc g++ python3-dev

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

STOPSIGNAL SIGINT
ENTRYPOINT ["python3", "-u", "main.py"]